# sudo as

Ensure a user can sudo as another user. Good for doing safe deployments of stuff using an unprivileged account.

# Variables

  - ``sudoas_user1`` - First user, is by default set to ``ansible_user_id``.
  - ``sudoas_user2`` - Second user, the one you sudo as, has no default set and must be set to avoid errors.
  - ``sudoas_nopasswd`` - Add NOPASSWD to the resulting sudoers config.
